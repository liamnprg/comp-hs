# comp-hs

Welcome to the Alang compiler project, this time written in haskell.

## Alang

Alang is a simple language I made up, it looks about like this:

a=3+5
b=a+2
c=a-1
d=b/2
e=d\*5

The language has five registers, all of which are shown above. 

## Compiler

This is a binary-building compiler that produces an a.out file using llvm.

## Supported targets

- x86[64],riscv[32/64]

You cannot currently compile for riscv\* if your llvm does not support it(most don't), or if your ld.lld does not support it(mine doesn't so it remains untested. I'm not sure if riscv support is even in lld).

## Requirements

the lld linker, a SINGLE INSTANCE OF LLVM, and the haskellstack

You also need to run `git clone git@gitlab.com:liamnprg/ir-shared.git` in order to access some shared code

## Using

Run `stack build` to build, then either `stack run`, or `stack exec comp-hs-exe -- sample.al --target <target triple of choice>`. For a more in-depth explanation, see `stack exec comp-hs-exe -- --help`

## FAQ

When compiling, I get the error: "ld.lld: error: cannot open ./ir-shared/libx86_64.o: No such file or directory", what do I do?

Make sure to clone the ir-shared repository and run the Makefile contained inside it
