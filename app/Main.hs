module Main where

import Lex
import Parse
import Cgen
import System.Console.ArgParser

data ProgArgs = ProgArgs String String
  deriving (Show) -- we will print the values

myTestParser :: ParserSpec ProgArgs
myTestParser = ProgArgs
  `parsedBy` reqPos "file" `Descr` "The .al file to compile"
  `andBy` optFlag "" "target" `Descr` "The target triple to compile for, if this argument is not present, native will be assumed"

main :: IO ()

main = do
   withParseResult myTestParser rmain

rmain (ProgArgs f t)  = do
  s <- readFile f
  let lexres=clex s
  --print lexres
  let parseres =cparse lexres
  --print parseres
  cgen parseres t
