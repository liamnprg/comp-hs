{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE OverloadedStrings #-}
module Cgen 
    ( cgen
    ) where


import Parse
import Lex
import System.Process
import Data.Word
import LLVM.AST hiding (function)
import Data.List.Split
import LLVM.AST.Type as AST
import qualified LLVM.AST.Float as F
import qualified LLVM.AST.Constant as C
import qualified LLVM.AST.IntegerPredicate as P
import Data.Map.Strict
import LLVM.Module
import LLVM.Internal.Target
import LLVM.Relocation
import LLVM.CodeModel
import LLVM.CodeGenOpt
import qualified Data.ByteString.Short as Sh

import qualified LLVM.AST as AST
import LLVM.AST.Global
import LLVM.Context

--import Control.Monad.Except

import LLVM.IRBuilder.Module
import LLVM.IRBuilder.Monad
import LLVM.IRBuilder.Instruction
import Data.ByteString.Char8 as BS

--{
--load libutil
--extract print_regs
--main
--  entry:
--  alloca a,b,c,d,e
--  function body gen
--  load a,b,c,d,e
--  pring_regs
--  ret void
--function body gen:
--  if arg1 is a reg:
--      build a constint and no load
--  if arg2 is a reg:
--      build a constint and no load
--  depending on oper, build
--  store value back where it came form
--}

isconst (Parse.Reg r) = False
isconst x = True

reg2reg x a b c d e
  | x == A = a 
  | x == B = b 
  | x == C = c
  | x == D = d
  | x == E = e

--check that the ir is up to spec, and fix this mempty case

bodygen :: MonadIRBuilder m => [Expr] -> Operand -> Operand -> Operand -> Operand -> Operand -> m ()
--bodygen :: MonadIRBuilder m => [Expr] -> Operand -> Operand -> Operand -> Operand -> Operand -> m Operand
bodygen [] _ _ _ _ _ = return ()
bodygen (x:xs) a b c d e = mdo
--    let (dst,a1,op,a2) = get x
    let (Expr dst a1 op a2) = x

    c1 <- if (isconst a1)
        then let (Parse.Const c) = a1 in do
            let cg = ConstantOperand $ C.Int (fromIntegral c) (fromIntegral c)
            --cg <- load a 0 
            return cg
        else let (Parse.Reg r) = a1 in do
            let reg2load =reg2reg r a b c d e
            rg <- load reg2load 0
            return rg

    c2 <- if (isconst a2)
        then let (Parse.Const c) = a2 in do
            let cg = ConstantOperand $ C.Int (fromIntegral c) (fromIntegral c)
            --cg <- load a 0 
            return cg
        else let (Parse.Reg r) = a2 in do
            let reg2load =reg2reg r a b c d e
            rg <- load reg2load 0
            return rg
    last <- genifromconst c1 op c2
    store (reg2reg dst a b c d e) 0 last

    bodygen xs a b c d e
    




--c1=const 1,op=oper,c2=const 2

genifromconst :: MonadIRBuilder m => Operand -> Operation -> Operand -> m Operand
genifromconst c1 op c2
  | op == Lex.Add = add c1 c2
  | op == Lex.Mul = sub c1 c2
  | op == Lex.Sub = mul c1 c2
  | op == Lex.Div = udiv c1 c2



simple :: [Expr] -> AST.Module
simple ast = buildModule "alang" $ mdo
  --function "main" [] AST.void $ \[a] -> mdo
  print_regs <- extern "print_regs" [AST.i32,AST.i32,AST.i32,AST.i32,AST.i32] AST.void
  function "main" [] AST.void $  \[] -> mdo
    _entry <- block `named` "entry"
    a <- alloca AST.i32 Nothing 0 `named` "a"
    b <- alloca AST.i32 Nothing 0 `named` "b"
    c <- alloca AST.i32 Nothing 0 `named` "c"
    d <- alloca AST.i32 Nothing 0 `named` "d"
    e <- alloca AST.i32 Nothing 0 `named` "e"

    no <- bodygen ast a b c d e
    
    la <- load a 0 `named` "la"
    lb <- load b 0 `named` "lb"
    lc <- load c 0 `named` "lc"
    ld <- load d 0 `named` "ld"
    le <- load e 0 `named` "le"
    _res <- call print_regs [(la,[]),(lb,[]),(lc,[]),(ld,[]),(le,[])]
    retVoid


pllvm :: AST.Module -> IO ()
pllvm mod = withContext $ \ctx -> do
  llvm <- withModuleFromAST ctx mod moduleLLVMAssembly
  BS.putStrLn llvm

genmods :: AST.Module -> String -> IO ()
genmods mod triple = withContext $ \ctx -> do
    withModuleFromBitcode ctx (File ("./ir-shared/libutil.bc" :: String))  $ (\lmod -> do
        withModuleFromAST ctx mod (\rmod -> do
            linkModules rmod lmod
            --tmod <- moduleLLVMAssembly rmod
            toasm rmod triple
            --BS.putStrLn tmod
            )

        )
--    Prelude.putStrLn ""
--"x86_64-pc-linux-gnu"    
--by switching the arch used here, you can switch which arch llvm is compiling for, implement me
toasm :: LLVM.Module.Module -> String -> IO ()
toasm mod triple = do
    let irpath = (\trip -> "./ir-shared/lib" ++ (Prelude.head $ splitOn "-" triple) ++ ".o")
    let sbs = Sh.toShort $ pack triple
    withTargetOptions (\tgt -> do
        initializeAllTargets
        -- is this actually an err?
        (rtgt,err) <- lookupTarget Nothing sbs
        --print err
        withTargetMachine rtgt sbs "generic" (Data.Map.Strict.empty) tgt LLVM.Relocation.Default LLVM.CodeModel.Default LLVM.CodeGenOpt.Default  $ (\machine -> do
           writeObjectToFile machine (File "/tmp/filehs.o") mod))
    
    createProcess (proc ("ld.lld") [irpath triple,"/tmp/filehs.o"])
    return ()






cgen :: [Expr] -> String -> IO ()
cgen ast target = do 
    --pllvm (simple ast)
    triple <- (if target == ""
                    then do
                        cstr <- getDefaultTargetTriple
                        let str =unpack $ Sh.fromShort cstr
                        return str
                    else do
                        return target)
    genmods (simple ast) triple



