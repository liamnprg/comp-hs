module Lex
    ( clex,
    Token(..),
    Register(..),
    Operation(..)
    ) where
data Register = A | B | C | D | E
  deriving (Show,Eq) 
data Operation = Add | Div | Mul | Sub
  deriving (Show,Eq) 
data Token = Reg Register | Eq | Const Int | Eof | Nl | Oper Operation
  deriving (Show,Eq)

data State = State Int

clex xs = clex_i xs 1

clex_i :: [Char] -> Int -> [Token]
clex_i (x:xs) line
  | x == '=' = Eq:(clex_i xs line)
  | isReg x = (Reg $ getReg x):(clex_i xs line)
  | isNum x = (Const (read [x] :: Int)):(clex_i xs line)
  | x == '\n' = Nl:(clex_i xs (line+1))
  | isOp x = (Oper $getOper x):(clex_i xs line)
  | otherwise = error ("Error on line " ++ (show line) ++ " unexpected character " ++ (show x))
clex_i _ _ = [Eof]

getOper x
  | x == '+' = Add
  | x == '/' = Div
  | x == '-' = Sub
  | x == '*' = Mul
  | otherwise = error "literally impossible to reach these lines of code"

isOp x 
  | x == '+' = True
  | x == '*' = True
  | x == '-' = True
  | x == '/' = True
  | otherwise = False

getReg :: Char -> Register
getReg x
  | x == 'a' = A
  | x == 'b' = B
  | x == 'c' = C
  | x == 'd' = D
  | x == 'e' = E
  | otherwise = error "Hey, wat you doin here"

isReg :: Char -> Bool
isReg x
  | x `elem` ['a' .. 'e'] = True
  | otherwise =  False

isNum x
  | x `elem` ['0' .. '9'] = True
  | otherwise = False
