module Parse
    ( cparse,
    Expr(..),
    Val(..),
    ) where

import Lex

data Expr = Expr Register Val Operation Val
  deriving (Show) 



rexpr v2 op v1 dst = Expr dst v1 op v2

data Val = Reg Register | Const Int 
  deriving (Show) 

cparse xs = cparse_i xs 1
cparse_i :: [Token] -> Int -> [Expr]
cparse_i [] _ = []
cparse_i xs s = let (e,ns,nxs) = dst xs Expr s in e:(cparse_i nxs ns)
--cparse_i xs s = 

-- This function might be called with only an "EOF", if this is the case, we can just return
dst :: [Token] -> (Register -> Val -> Operation -> Val -> Expr) -> Int -> (Expr,Int,[Token])
dst ((Lex.Reg r):xs) f s = eq xs (f r) s
dst (x:xs) _ s = err s x "Destination Register" 

eq :: [Token] -> (Val -> Operation -> Val -> Expr) -> Int -> (Expr,Int,[Token])
eq (Eq:xs) f s = fval xs f s
eq (x:xs) _ s = err s x "=" 

fval :: [Token] -> (Val -> Operation -> Val -> Expr) -> Int -> (Expr,Int,[Token])
fval ((Lex.Reg r):xs) f s = oper xs (f (Parse.Reg r)) s
fval ((Lex.Const i):xs) f s = oper xs (f (Parse.Const i)) s
fval (x:xs) _ s = err s x "Constant or Register" 

oper :: [Token] -> (Operation -> Val -> Expr) -> Int -> (Expr,Int,[Token])
oper ((Oper o):xs) f s = lval xs (f o) s
oper (x:xs) _ s = err s x "Operation" 

lval :: [Token] -> (Val -> Expr) -> Int -> (Expr,Int,[Token])
lval ((Lex.Reg r):xs) f s = nleof xs (f (Parse.Reg r)) s
lval ((Lex.Const i):xs) f s = nleof xs (f (Parse.Const i)) s
lval (x:xs) _ s = err s x "Constant or Register" 

nleof :: [Token] -> Expr -> Int -> (Expr,Int,[Token])
nleof (x:y:xs) ex s
  | x == Nl && y == Eof = (ex,s+1,xs)
  | x == Nl = (ex,s+1,y:xs)
  | otherwise = err s x "Newline or Eof" 

err s x msg =  error $ "error: On line " ++ (show s) ++ ", was expecting a(n) " ++ msg ++", but got " ++ (show x)
